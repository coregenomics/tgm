SOURCE := $(wildcard *.Rmd)
TARGET := $(SOURCE:.Rmd=.pdf)

R_PROFILE_USER := .Rprofile
R_LIBS_USER := rlib

.PHONY : pdf
pdf : $(TARGET)
$(TARGET) : %.pdf : %.Rmd %.mzn
	Rscript -e 'rmarkdown::render("$<")'

.PHONY : deps
deps :
	Rscript -e 'pkgs <- c("rticles", "showtext", "tidyverse"); suppressWarnings(to_install <- pkgs[! pkgs %in% library()$$results[, "Package"]]); if (length(to_install)) install.packages(to_install)'

.PHONY : local-libs
local-libs : $(R_PROFILE_USER) $(R_LIBS_USER)

$(R_PROFILE_USER) :
	echo 'local({options(Ncpus = parallel::detectCores());.libPaths("$(R_LIBS_USER)")})' > $@

$(R_LIBS_USER) :
	mkdir -vp $@
